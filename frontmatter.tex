% Title Page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% title of your thesis
% \title{Approaches for Complex Geometry Representation in High-Order Finite-Volume Schemes on Structured Grids}
% \title{Discrete Representation of Complex Geometries in High-Order Finite-Volume Schemes on Structured Grids with Adaptation}
\title{Geometry Considerations for High-Order Finite-Volume Methods on Structured Grids with Adaptive Mesh Refinement}
% author's name
\author{Nathaniel D. Overton-Katz}

% author's email address
\email{noverton@colostate.com}

% department name
\department{Department of Mechanical Engineering}

% semester of completion
\semester{Summer 2022}

% committee member names
\advisor{Stephen Guzik}
\coadvisor{Xinfeng Gao}
\committee{Chris Weinberger}
\committee{Wolfgang Bangerth}

% Copyright Page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% here is an example of student copyright declaration
% note that the \copyright command prints the copyright symbol,
% so we use the name \mycopyright instead
\mycopyright{%
Copyright by Nathaniel D. Overton-Katz 2022 \\
All Rights Reserved
}

% here is an example of a creative commons copyright license
% ask the graduate school for more information, if you are interested
%\mycopyright{%
%This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 United States License.
%
%\vspace{3em}
%
%To view a copy of this license, visit:
%
%\vspace{2em}
%
%\url{http://creativecommons.org/licenses/by-nc-nd/4.0/legalcode}
%
%\vspace{3em}
%
%Or send a letter to:
%
%\vspace{2em}
%
%Creative Commons
%
%171 Second Street, Suite 300
%
%San Francisco, California, 94105, USA.
%}

% Abstract
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\abstract{
  Computational fluid dynamics (CFD) is an invaluable tool for engineering design.
  Meshing complex geometries with accuracy and efficiency is vital to a CFD simulation.
  In particular, using structured grids with adaptive mesh refinement (AMR) will be invaluable to engineering optimization where automation is critical.
  For high-order (fourth-order and above) finite volume methods (FVMs), discrete representation of complex geometries adds extra challenges.
  High-order methods are not trivially extended to complex geometries of engineering interest.
  To accommodate geometric complexity with structured AMR in the context of high-order FVMs, this work aims to develop three new methods.

  First, a robust method is developed for bounding high-order interpolations between grid levels when using AMR.
  High-order interpolation is prone to numerical oscillations which can result in unphysical solutions.
  To overcome this, localized interpolation bounds are enforced while maintaining solution conservation.
  This method provides great flexibility in how refinement may be used in engineering applications.

  Second, a mapped multi-block technique is developed, capable of representing moderately complex geometries with structured grids.
  This method works with high-order FVMs while still enabling AMR and retaining strict solution conservation.
  This method interfaces with well\-/established engineering work flows for grid generation and interpolates generalized curvilinear coordinate transformations for each block.
  Solutions between blocks are then communicated by a generalized interpolation strategy while maintaining a single-valued flux.

  Finally, an embedded-boundary technique is developed for high-order FVMs.
  This method is particularly attractive since it automates mesh generation of any complex geometry.
  However, the algorithms on the resulting meshes require extra attention to achieve both stable and accurate results near boundaries.
  This is achieved by performing solution reconstructions using a weighted form of high-order interpolation that accounts for boundary geometry.

  These methods are verified, validated, and tested by complex configurations such as reacting flows in a bluff-body combustor and Stokes flows with complicated geometries.
  Results demonstrate the new algorithms are effective for solving complex geometries at high-order accuracy with AMR.
  This study contributes to advance the geometric capability in CFD for efficient and effective engineering applications.
}

% Acknowledgments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\acknowledgements{
  I would like to thank my advisors Dr. Guzik and Dr. Gao for their advice, support, and the opportunities for growth they have made available to me.
  I am gratefully for the patience and guidance they have given me as a student, and have become a better researcher and person thanks to the time spent working with them.
  From my committee I thank Dr. Weinberger for his feedback and suggestions, and Dr. Bangerth for continually pushing me to think critically.
  I especially want to thank Dr. Johansen and Dr. Antepara at LBNL for their insight and guidance with the embedded-boundary method and Chombo framework.
  It has been an honor to work with them, and their help has been invaluable.
  I thank my colleges in the CFD and Propulsion lab for their friendship, discussion, and collaboration.
  I would like to express my gratitude to my wife for her love, encouragement, and patience.
  Finally, I would like to express my appreciation to my parents, family, and friends for their support.

}

% \end{multicols}
% \renewcommand{\nomname}{List of Symbols}

% \renewcommand{\nompreamble}{The next list describes several symbols that will be used within the body of the document}

% \renewcommand\nomgroup[1]{%
%   \item[\bfseries
%   \ifstrequal{#1}{A}{Physics Constants}{%
%   \ifstrequal{#1}{B}{Number Sets}{%
%   \ifstrequal{#1}{C}{Other Symbols}{}}}%
% ]}

% \nomenclature{$c$}{Speed of light in a vacuum inertial frame}
% \nomenclature{$h$}{Planck constant}

% \opensymdef
% \newsym[Energy]{symE}{E}
% \newsym[Mass]{symm}{m}
% \newsym[Speed of light]{symc}{c}
% \closesymdef
