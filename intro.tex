\chapter{Introduction}
\label{chap:intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivations}
Computational fluid dynamics (CFD) has become an invaluable tool for engineering design, allowing for rapid analysis and design optimization~\cite{Slotnick2014}.
However, the accuracy of CFD modeling of practical fluid dynamics is dependent on many factors, such as the physics models employed in the governing equations, space and time discretization schemes, computational configurations of initial and boundary conditions, and the representation of complex geometry.

When modeling compressible fluid dynamics, one of the significant challenges is managing discontinuities and shocks.
Finite-volume methods (FVMs) are an especially good fit for solving flows that experience discontinuities because FVMs are inherently conservative.
FVMs have been well-developed for low-order accuracy~\cite{Pulliam2014}.
Modern research codes extend FVMs to high-order accuracy, that is $O(\dx^{\pOrder})$ with $\pOrder \ge 3$ and $\dx$ the spatial cell size.
High-order FVMs decrease the numerical errors far more rapidly as the mesh is refined for smooth flows.
Despite the improved accuracy and efficiency of high-order FVMs, they are less prevalent because they require more effort to understand and develop algorithms that are robust and efficient~\cite{Wang2013}.
High-order FVMs do require more computation per cell, although they also better utilize modern computer architectures.

Applying a high-order FVM to structured grids greatly facilitates the solution process, due to the simple data layout, straightforward solution reconstruction, and well\-/understood error properties.
% Significant computation efficiency is also gained by operating on structured grids because the implicit data layouts are easily stored, traversed, and parallelized.
% This is especially true for high-order schemes, where operations use large neighborhoods of data.
An additional motivation for using structured grids is to permit efficient adaptive mesh refinement (AMR).
AMR is a method of allowing refinement to be added in local regions of interest while the solution is running, which allows for higher solution accuracy with reduced computational expense.
AMR at high-order is viable on structured grids due to a straightforward process of refining and coarsening~\cite{McCorquodale2011}.
Nevertheless, structured grids do have a significant drawback --- representing complex geometries is difficult, especially at high-order.
Therefore, this research aims to develop efficient and accurate geometric capabilities to represent complex geometries for high-order FVMs on structured grids with AMR.

% \section{Literature Review}
% \fix{Add stuff here}
% % What I am using
% % - Guzik mapping paper, and Chord refs
% % - MMB chombo ref
% % - Chombo EB ref
% % Other methods
% % - overture
% % - HO unstructured
% % - HO weno
% % - eb

% % The CFD \& Propulsion Lab's in-house CFD software, Chord~\cite{Gao2014b,Guzik2015,Guzik2016,Gao2016a,Owen2018}, is one such CFD code that achieves high-solution accuracy and efficiency.
% % Chord is a high-order finite-volume method (FVM) that employs AMR on structured grids, and solves complex compressible flows involving turbulence and combustion.
% % This combination of features in Chord allow it to solve complex physics with both a great degree of accuracy and computationally efficiency.

\section{Objectives}
The goal of this work is to enable high-ordered finite-volume methods to operate on structured grids with AMR for complex geometries of interest to practical engineering.
The challenge is --- how to effectively represent complicated geometries using a structured grid with AMR.
To overcome the challenge and achieve this goal, three distinct methods are defined.
First, we develop a strategy for more robust AMR, allowing for use in a larger variety of engineering applications.
% Following this, two methods for representing complex geometries with structured grids are explored, the mapped multi-block (MMB) and embedded-boundary (EB) methods.
% Both of these methods are compatible with the existing code infrastructure in Chord, but have distinct advantages and challenges to overcome.
% MMB methods support highly accurate geometries but have geometric limitations and creating meshes requires both expertise and time.
% EB methods are less accurate for the same resolution and requires more care to develop, but permit complete automation of the meshing process for any geometry.
Second, we develop the mapped multi-block (MMB) method to represent complex geometries defined by discrete grids. % while working with the high-order FVMs and AMR.
The MMB method supports high-order accurate geometries with AMR but has geometric limitations, in addition to the fact that creating meshes requires both expertise and time.
Third, the embedded-boundary (EB) method is developed for high-order FVMs as a promising new approach for highly complex geometry representation where the MMB method would encounter difficulties.
EB methods are less accurate for the same resolution than the MMB method due to the cut cells on the boundaries.
However, the EB method permits complete automation of the meshing process for any geometry.
Development for each of these three methods not only stem from a common goal but also a shared set of mathematical tools.
% Fundamentally, each of these methods is achieved by careful application of high-order interpolation.

Specifically, the primary objectives of this research are:
\begin{enumerate}
  \item Develop a robust method for high-order AMR.
        This addresses the stability challenges faced by high-order interpolation across AMR levels and at multi-block boundaries.
        This is important for applications involving multi-scales and multi-physics such as turbulent combustion.
  \item Establish a standard CFD workflow for mapped structured grids that permits high-order accuracy and AMR.
        This entails the use of grids that are generated by common mesh generation software where it is standard practice to produce grids defined by discrete points.
        In order to effectively apply AMR and high-order features, a mapping function describing the shape of the structured grid is required to be constructed from a discrete grid.
  \item Enable the MMB method to represent general geometries of moderate complexity.
        This method should maintain fourth-order accuracy for smooth flows, strictly maintain conservation, and operate with AMR.
        The approach taken to achieve this is to extend an existing conforming mapped multi-block method from operating on smooth analytic geometries to general geometries coming from external mesh generation software.
  % \item Explore the EB method as a candidate for accommodating highly-complex geometries at high-order accuracy.
  %       In the CFD community, development of EB methods is an active research area due to their ability to completely automate the mesh generation process even for challenging geometries.
  %       % However, high-order methods are largely an undeveloped area of research, and to my knowledge have not been demonstrated for time evolving solutions outside this work.
  \item Extend the EB method at fourth-order accuracy to solve the Stokes equations while accommodating highly-complex geometries.
        In the CFD community, development of EB methods is an active research area due to their ability to completely automate the mesh generation process even for challenging geometries.
        % However, high-order methods are largely an undeveloped area of research, and to my knowledge have not been demonstrated for time evolving solutions outside this work.
  \item Demonstrate that with the development of the new methods in this dissertation, complex fluid flows influenced by geometries are possible with the fourth-order FVM on structured grids with AMR.
\end{enumerate}

\section{Dissertation Organization}
This dissertation is structured as follows.
The mathematical and numerical framework supporting this research is presented in \chapref{chap:background} for completeness and convenience.
% The following three chapters each describe a specific method that allow for the high-order finite volume scheme to operate in more complicated configurations.
In \chapref{chap:clipping}, the new adaptive clipping-and-redistribution method is described and discussed, which improves the robustness of AMR by providing conservative bounds-preservation for multi-dimensional interpolation.
When using AMR with a high-order FVM, high-order numerical interpolation between different levels of refinement is required.
However, this interpolation sometimes leads to numerical issues when physical bounds are violated.
The new method overcomes two major challenges of the high-order interpolation method.
First, the new method is bound-preserving near extrema or discontinuities to prevent the emergence of unphysical oscillations while maintaining the fourth-order accuracy in smooth flows.
Second, the new method satisfies the conservation requirement in multiple dimensions, particularly in the context of generalized curvilinear coordinate transformations.
Additionally, the new method is designed to be localized and computationally inexpensive.
% The new interpolation scheme is implemented in Chord and demonstrated by solving reacting flows, which are extremely sensitive to unphysical overshoots in conserved quantities.
% The test problems are shock-induced \ce{H2}-\ce{O2} combustion and a \ce{C3H8}-air flame in a practical bluff-body combustor.
% Results show that the method does not produce new extrema near discontinuities while maintaining high-order accuracy in smooth regions.
% In particular, the method is extremely beneficial for combustion with stiff chemistry.
% With the newly developed method, even if flame fronts cross AMR interfaces or if new grids are created in the vicinity of the flame, the solution stability is retained along with expected accuracy.
%
\chapref{chap:mmb} presents the conforming MMB method for handling structured grids that are discretely defined for general complex geometries.
The method effectively represents complex geometries by connecting multiple structured grids along block boundaries.
It is designed for high-order FVMs with AMR and maintains strict solution conservation over block boundaries by extending the mapping into ghost cells and interpolating ghost cells between blocks.
% In previous applications, the conforming mapped multi-block method with AMR has been restricted to selectively chosen analytically described geometries.
This dissertation extends the existing conforming mapped multi-block method for particular geometries that are analytically defined, to operate on arbitrary geometries defined by discrete data points.
% The new method for high-order AMR on discrete grids is enabled by producing a B-spline interpolation function for each block.
% Ghost cell interpolation between blocks is addressed in the presence of boundaries, and logistical challenges for interpolating ghost cells in arbitrary geometries are overcome.
%
In \chapref{chap:hoeb} the fourth-order Cartesian EB method is presented.
% The algorithm represents complex geometries on a Cartesian embedded-boundary
% grid, and takes special steps to achieve stable and accurate discretizations.
The method employs a weighted least-squares technique for spatial discretization to mitigate the ``small cut cell'' problem, without mesh modifications, cell merging, or redistribution.
% Solutions are advanced in time using a projection method coupled with a higher-order additive implicit-explicit (ImEx) Runge-Kutta method, where the stiff and non-stiff terms are treated implicitly and explicitly, respectively.
% This algorithm is implemented as a new code build using the Chombo framework, with its intention primarily as a pathfinder project.
% The formal accuracy of the method is demonstrated with numerical convergence
% tests.
% Results for more complex flows in boundary layers and around bluff bodies are demonstrated.
Finally, \chapref{chap:conclusion} concludes the dissertation by summarizing the original contributions and novelty of work, and proposes potential directions for future development.
