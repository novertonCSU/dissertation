\def\havefulldoc{1}
\input{preamble}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                              %%
%% A COVER                      %%
%%                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{cover}
\cleardoublepage

\begin{preliminary}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                              %%
%% SUMMARY                      %%
%%                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\phantomsection
\addcontentsline{toc}{section}{Project Summary}
\input{summary}
\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                              %%
%% CONTENTS                     %%
%%                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\phantomsection
\addcontentsline{toc}{section}{Table of Contents}
\tableofcontents
\cleardoublepage

\end{preliminary}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                              %%
%% Body                         %%
%%                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivations}
The accuracy of CFD modeling and simulation of practical fluid dynamics problems is critically dependent on many factors, such as physics models employed in the governing equations, space and time discretization schemes, computational configurations (initial and boundary conditions), and the representation of complex geometry.
Accurately representing complex geometries is challenging for many CFD solvers.
This is especially true when using high-order discretization schemes and structured adaptive mesh refinement (AMR) for computational accuracy and efficiency.
Currently, many CFD codes have some of these capabilities, but few codes have all the capabilities.
The CFD \& Propulsion Lab's in-house CFD software, Chord, is a high-order finite-volume method (FVM), employs structured grids with AMR, and solves complex compressible flows involving turbulence and combustion.
The present work provides a capability for solving complex geometry in Chord for practical engineering applications and two techniques are considered: mapped multi-block (MMB) and embedded boundary (EB).
Both are compatible with the existing code infrastructure.
MMB supports high accuracy but creating meshes requires expertise and time.
EB is less accurate, for the same resolution, but permits complete automation of the meshing process.

% High-order finite-volume methods (FVMs) are essential for obtaining numerical solutions of fluid dynamics problems where conservation and efficiency are desired.
% Structured Cartesian grids are attractive because they have implicit data layouts that allow for high computational efficiency and parallelization.
% More importantly, they are well suited for adaptive mesh refinement (AMR) due to the practical approach for regridding.
% For non-smooth flows or flows containing shock waves, the piecewise parabolic method (PPM) is enabled for better shock-capturing capability.
% The motivation of this work is to expand upon Chord to solve more complex flow configurations.

\section{Research Objectives}
Four research objectives have been identified that make full progress in using the MMB technique and preliminary progress in using EB.
\begin{enumerate}
\item The first task is to establish a standard CFD workflow for complex geometries that permits high-order accuracy and AMR.
This allows Chord to use grids that are generated by common mesh generation software.
There, standard practice is to produce grids defined at discrete points.
In order to effectively apply the AMR and high-order features of Chord, it is required to have a mapping function describing the shape of the structured grid.
This is accomplished by defining a high-order ($\ge 5$) volumetric interpolation function from the discrete points provided by the mesh generation software.
\item Second, a mapped multi-block (MMB) technique is developed to represent general geometries of moderate complexity with boundary conditions.
Requirements that must be satisfied for this method are that the method should maintain fourth-order accuracy for smooth flows, maintain conservation, and effectively use AMR.
For this, the approach is to extend an existing conforming mapped multi-block method to general geometries.
% Combining with the prior objective of following classic engineering work flows, significant logistical challenges have had to be overcome when using the mapped multi-block method.
\item The third objective is to develop a method for robust application of AMR on multi-block grids.
This addresses challenges faced by high-order interpolation across AMR levels and at multi-block boundaries.
This is especially important for applications with turbulent combustion, because chemically reacting flows are highly sensitive to instabilities resulting from numerical overshoots.
%In order to correct this, a new method for performing a bounded form of high-order interpolation has been devised.
\item The fourth objective is to explore an embedded boundary (EB) method as a candidate for accommodating highly-complex geometries.
In the CFD community, development of EB methods has been an active research area due to the major advantage that they can completely automate the mesh generation process even for very challenging geometries.
%Despite this, high-order developments of this technology have been challenging and yet to show success in fluid dynamics applications.
% Exploratory steps are taken towards making this method possible for Chord.
\end{enumerate}

\section{Research Progress to Date}
To date, research objectives one through three are nearly completed and the main progress is summarized here.

\subsection{Literature Review}
To place the importance of present research in the context, literature review was fully conducted and succinctly provided here.

Mapped structured grids have been shown as an effective method for representing geometries, although when using AMR require definition of a mapping function.
Custom methods for managing complex mapping functions are possible~\cite{Henshaw1996b}, but do not abide by standard CFD workflows where grids are defined at discrete data points from mesh generation software.
To allow AMR on grids defined at discrete points, approaches which create an interpolation as the grid mapping function have shown good results at second order accuracy~\cite{Steinthorsson1994, Kravchenko1999}.
Extending this approach to high-order methods has yet to be shown in literature.
The approach taken in this work is to create grid interpolation functions by methods of volumetric b-splines due to their well behaved error properties~\cite{DeBoor, Piegl1997}.

A wealth of literature exists on varied approaches for mapped multi-block methods. These methods \cite{Henshaw1996} have been able to capture highly complex geometries using overlapping grids, but may compromise on conservation. When devising these methods for high-order schemes it is challenging to maintain solution conservation and have reasonable approaches for interpolating data between blocks. One approach that accomplishes this is the conforming mapped multi-block method by McCorquodale et al. \cite{McCorquodale2015}. However, this approach has previously only been developed for nicely chosen analytic mapped grids and not applied for general geometries.

% Methods for robust AMR require forms of bounded high-order interpolation that maintain solution conservation. In the FVM community, high-order limiters~\cite{Colella2008} are well developed to bound interpolation of face quantities. However these methods generally do not account for interpolation in multiple dimensions or conservation.
Regarding bounded interpolations, an approach that is multi-dimensional, conservative, and solves the spatial interpolation problem faced by AMR is the CENO method described by Ivan et al.~\cite{Ivan2015}. Although it is an effective approach, it depends upon a tuned coefficient and large stencils of data can be required which necessitate expensive data communication.
Another approach for the multi-dimensional, conservative interpolation problem is the clipping-and-redistribution algorithm described by Hilditch et al.~\cite{Hilditch1997}.
%After a high-order interpolation, any solution outside physically imposed bounds is clipped off. The sum of clipped quantities is tracked and redistributed elsewhere inside the domain to maintain conservation.
This approach is developed to prevent physically constrained overshoots based upon predetermined bounds. Additionally, the redistribution happens globally and thus requires solving an implicit system which is potentially expensive. Nevertheless, this is the inspiration for the bounded interpolation algorithm developed by this research.
%We seek to adaptively enforce bounds near discontinuities and reduce the cost of redistribution. In addition, the present algorithm operates on mapped quantities since the underlying CFD infrastructure uses mapped structured grids to accommodate complex geometries.

The high-order embedded boundary method is an area of active research, but as yet has not been shown for time dependent solutions. Recent development for high-order methods have been shown for steady state problems by Devendran et al. \cite{Devendran2017}. At low-order, a significant number of sources have developed successful methods \cite{Graves2013, Aftosmis2000, Richards2021, Berger2012}. Note that the immersed boundary method, successful for high-order solutions of complex flows using finite difference methods \cite{Khalili2019}, is not the explicit geometric methods considered here. The immersed boundary method uses implicit functions to represent the interface which is diffusive and the solution is not conservative.

The present research is focused on the development of geometric capabilities that are high-order accurate, conservative, efficient, and support AMR. This work fills a gap in the current CFD field.

\subsection{Numerical Tools}
The first product resulting from the proposed research consists of a suite of numerical algorithms associated with the MMB technique. These have been developed and implemented in Chord. They have been verified and validated using cases from the literature. The final algorithms are applied to solve practical problems and the findings are compared to experimental data; for example, a bluff body combustor case which is actively used by the community to demonstrate the code capabilities, is tested. All these numerical algorithms and codes are managed in our code repository located at \url{https://cfd-repo.engr.colostate.edu}.
Code commits are made on daily basis.

\subsection{Scholarly Publications}
A sequence of publications in both journal and conference papers were disseminated.
The objective for Chord to follow a standard engineering workflow has largely been achieved and presented for single mapped grids in a conference paper \cite{Overton2017}. This approach was then extended to the mapped multi-block method and presented at a second conference. A corresponding paper was published \cite{Overton2020}. A first-authored journal paper reports a new method for robust application of AMR using a proposed high-order adaptive clipping-and-redistribution method and is under review \cite{Overton2021a}. Further developments of the new approach for enabling Chord to operate on mapped multi-block (MMB) methods is reported in a journal paper in progress \cite{Overton2021b} which details the application of the mapped multi-block method for general geometries. Additionally, contributions and support were made to a series of papers co-authored with colleagues \cite{Gao2015a, Gao2016c, Gao2017DAa, Wang2020}.

\section{Proposed Research for Thesis Completion}
To emphasize, this proposal is focused on articulating the research plan for the remaining tasks: the EB work and further improvements to the MMB method.

\subsection{Objectives and Goals}
The main objective of the remaining work focuses on research of the EB method as an alternative approach for creating complex geometry more efficiently for engineering applications.
The goal is to implement the EB method into Chord to solve compressible fluid flows.
Specifically, the objectives of this research are to demonstrate time-dependent diffusion and advection equations using high-order embedded boundary methods.

The second objective is to further mature the interpolations in the MMB method for more general use.
The goal is to improve solutions of turbulent combusting flows on complex geometries.
This is achieved by using clipping and redistribution algorithms, similar to what was used for AMR, and to use smaller interpolation regions when possible.

\subsection{Technical Tasks}
To successfully achieve the proposed objectives, concrete technical tasks are planed.

\begin{enumerate}
  \item For the EB research, it is required to demonstrate solutions using time-dependent diffusion and advection equations using high-order embedded boundary methods, and specific tasks are defined as follows.
        \begin{enumerate}
          \item Demonstration of a high-order embedded boundary method for solving the time-dependent diffusion equation using an implicit time marching scheme and featuring AMR.
                \begin{itemize}
                  \item There are significant challenges regarding appropriate interpolation of data in cut cells along boundaries and stability of solutions with these small cells.
                  \item High-order time dependent solutions have yet to be demonstrated in literature. Knowing this, even a low-order in time EB scheme is satisfactory.
                  \item The inclusion of AMR also requires additional care when implementing time dependent solutions.
                \end{itemize}
          \item Demonstration of a high-order embedded boundary method for solving the advection equation using explicit time marching.
                \begin{itemize}
                  \item Special care is required to design a scheme which is able to manage the time constraints imposed by small cells.
                  \item In the literature there is no clear consensus on the approach for explicit time marching schemes for embedded boundary methods of low-order, let alone high-order. Knowing this, use of even an implicit low-order-in-time EB scheme will be satisfactory.
                  \item Additionally, all the challenges faced when solving the diffusion equation are still of some concern.
                \end{itemize}
        \end{enumerate}
  \item The tasks to improve the mapped multi-block method are listed here:
        \begin{enumerate}
          \item Improve stencil stability by implementation of a weighted least squares method for the interpolation of ghost cells in the mapped multi-block approach.
                This approach has been developed in literature for related applications and shown to provide more localized interpolation less prone to high-order oscillations.
          \item Use the weighted least squares method to automatically detect conforming ghost cells and reduce the interpolation stencils in these regions.
                This approach aims to both increase the quality of interpolation and reduce the size of interpolation stencils when possible.
          \item Include a form of bounded interpolation for ghost cells in the mapped multi-block method. This will provide additional stability for reacting flows which are highly sensitive to small oscillations from numerical interpolation.
          % \item Interpolating ghost cells for mapped mutli-block methods featuring AMR is a logistically challenging problem. The existing approach for this is poorly optimized and can use very large stencils.
        \end{enumerate}
\end{enumerate}

\subsection{Research Plan}
A proposed timeline to complete the remaining work is shown in Figure \ref{fig:gantt}. In the figure, the green shows the completion of the task, the gray shows the remaining tasks, and the blue shows milestones.
% Beginning with the MMB research, the aim is to complete a significant amount of the technical tasks in the months of April and May 2021. In May, I begin an internship at Lawrence Berkeley National Laboratory (LBNL), where I plan to work with collaborators to progress EB research. While this research is in progress, a journal paper detailing the methods can take shape and hopefully be submitted for review in the fall of 2021. In August, I return to CSU, at which point research on the MMB methods will resume. Pending completion of the technical tasks, results from the MMB method can be generated and processed. This work will also be summarized in a journal paper to submit in the fall. To satisfy the requirements of a PhD, a dissertation is required. Significant work towards completing my dissertation will begin in August, and is aimed for completion early in 2022. Once the dissertation is nearing completion, the final defense can be scheduled and preparations made.
The timeline of the proposed work is broken into three major categories, the EB work, MMB work, and finally, work towards completion of a dissertation. The timeline for each of these is further explained
\begin{itemize}
  \item EB work
        \begin{itemize}
          \item Will continue the collaboration with Dr. Hans Johansen at Lawrence Berkeley National Laboratory (LBNL) from May to August as a summer intern. %During this, I plan to work with collaborators to develop the EB work.
          \item Both of the technical tasks for the EB research are planned to be completed during work from May through August. Some preliminary work has already been done towards accomplishing these tasks.
          \item A journal paper detailing the EB research is targeted to be submitted by September.
        \end{itemize}
  \item MMB work
        \begin{itemize}
          \item From April through May, the goal is to complete all the tasks involving stencil improvements of the MMB work.
          \item Resuming in September through November, the plan is to generate and process results from MMB work to ensure the work has been successful. Some of these results have already been obtained.
          \item Pending generation and processing of results, a journal paper is to be completed detailing the MMB work. This is largely complete and awaiting results.
        \end{itemize}
  \item Dissertation
        \begin{itemize}
          \item The target date to defend is in February 2022.
          \item Beginning in August, significant effort towards writing my dissertation is planned, with the goal of being completed by February 2022. A rough draft has been started.
          \item Once the dissertation is nearing completion, two months are allotted to plan and prepare before the final defense.
        \end{itemize}
\end{itemize}

\begin{figure}[!ht]
  \centering
  \includegraphics{figures/prelimGanttChart}
  \caption{An anticipated timeline of the remaining work to be completed.}
  \label{fig:gantt}
\end{figure}

\subsection{Measures for Success}
To measure whether the tasks are successfully accomplished, specific measures are designed.
Measuring success when developing these methods is primarily done by approaches of verification and validation.
%
Verification of a method ensures that the mathematics are solved correctly and
the designated numerical properties are satisfied. Confirming a solution is
fourth-order accurate requires first determining an exact reference solution for
a certain case configuration. Using this, a series of cases on successively
refined grids are solved and each have the solution error measured relative to
the exact solution. As grids are refined, the rate that solution errors decrease
is measured and should match the expected fourth-order of the scheme. For
verification of complex cases, manufactured solutions can be created to serve as
the exact solutions and are satisfied with prescribed boundary conditions and
source terms. Solution conservation is determined by comparing a summation of
the solution quantities at different points in time and ensuring these quantity
remains constant when source terms are not present.
%
Validation ensures that a developed method is able to appropriately represent physics of interest.
Performing validation is done by comparing a solution for a developed method to that of solutions in literature and ensuring that key physical quantities match well.
This can be done using reference data from both physical experiment and previously validated numerical solutions.

% For the proposed mapped multi-block work, success will be measured by demonstrating that the developed method is fourth-order accurate, conservative, and includes AMR for solutions of the Navier-Stokes equations.
% Additionally, the target is to show the method is stable for solutions involving shocks and combustion.
% This requires verification that the solution is fourth-order and conservative for representative cases where exact solutions are known.
% Validation will be performed on problems containing shocks by comparing to existing single-block methods.
% Validation will also be performed for cases with complex geometries and flows involving combustion by comparing numerical solutions existing literature and experimental data.

% For the embedded boundary work, success will be measured by demonstrating a developed solution to the diffusion and advection equations that is fourth-order accurate, conservative, and includes AMR.
% This requires verification of the prescribed numerical properties on domains including complex geometries.
% An approach for devising exact solutions in the presence of complex geometries will be to use manufactured solutions that are satisfied with prescribed boundary and source terms.
% Validation will also be performed by comparing the solutions of the developed work to existing low-order numerical solutions in literature.
% Ideally the newly developed high-order approach will be able to show improvements for solutions of comparable resolution.

Measures for success of each technical task are outlined in further detail.
\begin{enumerate}
  \item For the EB research, it is required to demonstrate the time-dependent diffusion and advection equations using high-order embedded boundary methods. Specific measures for success of this are defined as follows.
        \begin{enumerate}
          \item Demonstration of a high-order embedded boundary method for solving the time-dependent diffusion equation using an implicit time marching scheme and featuring AMR.
                \begin{itemize}
                  \item Verification of high-order in space for smooth solutions.
                  \item Ideally the method is verified as high-order in time using an implicit method, although a second-order in time method may be sufficient.
                  \item The method is validated to cope with highly complex geometries by comparing to low-order numerical results in literature.
                \end{itemize}
          \item Demonstration of a high-order embedded boundary method for solving the advection equation using an explicit time marching scheme and featuring AMR.
                \begin{itemize}
                  \item Verification of high-order in space for smooth solutions.
                  \item Ideally the method is verified as high-order in time using an explicit method, although a second-order in time implicit method may be sufficient.
                  \item The method is validated to cope with highly complex geometries by comparing to low-order numerical results in literature.
                \end{itemize}
        \end{enumerate}
  \item The tasks to complete for the mapped multi-block method with specific measures are listed here:
        \begin{enumerate}
          \item Improved stability by implementation of a weighted least squares method for the interpolation of ghost cells.
                \begin{itemize}
                  \item Verification of high-order for smooth solutions.
                  \item Analysis of eigenvector properties to determine increased stability properties.
                  \item Validation of the method by comparing to previously validated single-block solutions of the Navier-Stokes equations.
                \end{itemize}
          \item Automatic detection of conforming ghost cells and reduction of interpolation stencils in these regions.
                \begin{itemize}
                  \item Verification of high-order for smooth solutions.
                  \item Verification of proper engagement of the method to reduce stencils when appropriate.
                  \item Measured reduction in computational cost of interpolation for regions the method is applied.
                \end{itemize}
          \item Bounded interpolation of ghost cells.
                \begin{itemize}
                  \item Verification of high-order for smooth features.
                  \item Verification of bounds preservation for discontinuous features.
                  \item Validation of a high-order turbulent combustion flow enabled by this work. This will be compared to existing literature from numerical and experimental sources.
                \end{itemize}
        \end{enumerate}
\end{enumerate}


\cleardoublepage
\appendix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                              %%
%% Appendix A                   %%
%%                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\input{appendix/appendix}
%\cleardoublepage

\input{postamble}
