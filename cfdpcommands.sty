\ProvidesPackage{cfdpcommands}[2014/07/11 v1.0]
\RequirePackage{xspace}      % Spacing after some new commands
\RequirePackage{amsmath}     % boldsymbol
\RequirePackage{amssymb}     % mathbf
\RequirePackage{bm}          % Reimlements /boldsymbol
\RequirePackage{upgreek}     % \upPi and family

\makeatletter

%--Main doc or sub doc
\newif\iffulldoc
\ifx\havefulldoc\undefined
  \fulldocfalse
\else
  \fulldoctrue
\fi

%--Preliminary environment
\newenvironment{preliminary}%
  {\pagestyle{plain}\pagenumbering{roman}}%
  {\cleardoublepage\pagenumbering{arabic}}

%--Roman numerals
\newcommand{\rmnum}[1]{\romannumeral #1}
\newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}

%--Create a \deg command
\DeclareMathSymbol{\circx}{\mathord}{symbols}{"0E}
\let\olddeg=\deg
\renewcommand\deg{\ensuremath{\,^\circx\,}}

%--A nice C++ symbol
\newcommand*{\Cpp}{C\parbox{1em}{\vspace*{-0.6ex}$\scriptstyle\mspace{-1mu}+\mspace{-1mu}+$}\xspace}%

%--This section creates the \un command for units and \E for exponents
%--E.g., 2.9\un{m^{2}} or 2.9\E{-3}\un{m^{2}}
%--Can be used in or out of math environment (math ensured in braces)
%--also a \un* for no spacing
\DeclareMathSymbol{\timesx}{\mathord}{symbols}{"02}
\newcommand{\etimes}{\mskip1.1mu\timesx\mskip0.9mu\relax}
\newcommand{\E}[1]{\ensuremath{\etimes10^{#1}}}

\newmuskip\unitmuskip
\newmuskip\unitpreskip
\unitmuskip=1mu plus 0.3mu              % Change this to suit your preference
\unitpreskip=4mu plus 1mu minus 1mu     % Change this to suit your preference
\def\unitmskip{\penalty10000 \mskip\unitmuskip}%
\DeclareRobustCommand\un{\@ifstar{\@unstar}{\@un}}
\newcommand\@unstar[1]{\leavevmode
  \ensuremath{%
    \begingroup \fam\z@%
    \let\,\unitmskip \unit@PreserveSpaces\@empty #1 \unit@PreserveSpaces%
    \endgroup}}%
\newcommand\@un[1]{\leavevmode%
  \unskip\penalty1000%
  \ensuremath{%
    \@checkfordeg #1\@empty%
    \begingroup \fam\z@%
    \let\,\unitmskip \unit@PreserveSpaces\@empty #1 \unit@PreserveSpaces%
    \endgroup}}
\def\unit@PreserveSpaces#1 {#1\@ifnextchar\unit@PreserveSpaces{\@gobble}%
  {\unitmskip \unit@PreserveSpaces\@empty}}%
\def\@checkfordeg{\@ifnextchar\deg{\@junkrest}{\mskip\unitpreskip\@junkrest}}%
\def\@junkrest#1\@empty{}%

%--Corrections and notes
\newcommand{\mnote}[1]{\marginpar{\textcolor{red}{#1}}}%
\newcommand{\inote}[1]{\textcolor{red}{[#1]}}%
\newcommand{\fnote}[1]{\footnote{\textcolor{red}{#1}}}%

%--Compact enumeration
\newenvironment{compactenum}
  {\begin{enumerate}{}
    \setlength{\parsep}{0pt}
    \setlength{\itemsep}{0.2ex}
  }
  {\end{enumerate}}

%--Compact itemize
\newenvironment{compactitem}
  {\begin{itemize}{}
    %\setlength{\topsep}{0pt}
    \setlength{\parsep}{0pt}
    \setlength{\itemsep}{0.5ex}
    \setlength{\leftmargin}{1.5em}
    \setlength{\itemindent}{1.0em}
  }
  {\end{itemize}}

%--Compact itemize for function arguments
\newenvironment{argitem}{\begin{list}{{\footnotesize$\circ$}}
  {\setlength{\topsep}{0pt}
    \setlength{\parsep}{0pt}
   \setlength{\itemsep}{0.5ex}
   \setlength{\leftmargin}{2em}
   \setlength{\itemindent}{-1em}}
  }
{\end{list}}

%--Location which changes if full or chapter compilation
\iffulldoc
  \newcommand*{\pathpre}[1]{#1}
\else
  \newcommand*{\pathpre}[1]{../#1}
\fi

%--Consistent references
\newcommand{\figref}[1]{Figure~\ref{#1}} 
\newcommand{\tblref}[1]{Table~\ref{#1}}
\newcommand{\secref}[1]{Section~\ref{#1}} 
\newcommand{\chapref}[1]{Chapter~\ref{#1}} 
\newcommand{\appref}[1]{Appendix~\ref{#1}} 
\newcommand{\eqnref}[1]{Equation~\ref{#1}}
\newcommand{\eqnsref}[1]{Equations~\ref{#1}}

%--Commonly used math notation
% Plain text characters (\<letter>rm)
\newcommand{\Arm}{{\text{A}}}
\newcommand{\Brm}{{\text{B}}}
\newcommand{\Crm}{{\text{C}}}
\newcommand{\Drm}{{\text{D}}}
\newcommand{\Erm}{{\text{E}}}
\newcommand{\Frm}{{\text{F}}}
\newcommand{\Grm}{{\text{G}}}
\newcommand{\Hrm}{{\text{H}}}
\newcommand{\Irm}{{\text{I}}}
\newcommand{\Jrm}{{\text{J}}}
\newcommand{\Krm}{{\text{K}}}
\newcommand{\Lrm}{{\text{L}}}
\newcommand{\Mrm}{{\text{M}}}
\newcommand{\Nrm}{{\text{N}}}
\newcommand{\Orm}{{\text{O}}}
\newcommand{\Prm}{{\text{P}}}
\newcommand{\Qrm}{{\text{Q}}}
\newcommand{\Rrm}{{\text{R}}}
\newcommand{\Srm}{{\text{S}}}
\newcommand{\Trm}{{\text{T}}}
\newcommand{\Urm}{{\text{U}}}
\newcommand{\Vrm}{{\text{V}}}
\newcommand{\Wrm}{{\text{W}}}
\newcommand{\Xrm}{{\text{X}}}
\newcommand{\Yrm}{{\text{Y}}}
\newcommand{\Zrm}{{\text{Z}}}
\newcommand{\Alpharm}{{\upAlpha}}
\newcommand{\Betarm}{{\upBeta}}
\newcommand{\Gammarm}{{\upGamma}}
\newcommand{\Deltarm}{{\upDelta}}
\newcommand{\Epsilonrm}{{\upEpsilon}}
\newcommand{\Zetarm}{{\upZeta}}
\newcommand{\Etarm}{{\upEta}}
\newcommand{\Thetarm}{{\upTheta}}
\newcommand{\Iotarm}{{\upIota}}
\newcommand{\Kapparm}{{\upKappa}}
\newcommand{\Lambdarm}{{\upLambda}}
\newcommand{\Murm}{{\upMu}}
\newcommand{\Nurm}{{\upNu}}
\newcommand{\Xirm}{{\upXi}}
\newcommand{\Pirm}{{\upPi}}
\newcommand{\Rhorm}{{\upRho}}
\newcommand{\Sigmarm}{{\upSigma}}
\newcommand{\Taurm}{{\upTau}}
\newcommand{\Upsilonrm}{{\upUpsilon}}
\newcommand{\Phirm}{{\upPhi}}
\newcommand{\Chirm}{{\upChi}}
\newcommand{\Psirm}{{\upPsi}}
\newcommand{\Omegarm}{{\upOmega}}
\newcommand{\arm}{{\text{a}}}
\newcommand{\brm}{{\text{b}}}
\newcommand{\crm}{{\text{c}}}
\newcommand{\drm}{{\text{d}}}
\newcommand{\erm}{{\text{e}}}
\newcommand{\frm}{{\text{f}}}
\newcommand{\grm}{{\text{g}}}
\newcommand{\hrm}{{\text{h}}}
\newcommand{\irm}{{\text{i}}}
\newcommand{\jrm}{{\text{j}}}
\newcommand{\krm}{{\text{k}}}
\newcommand{\lrm}{{\text{l}}}
\newcommand{\mrm}{{\text{m}}}
\newcommand{\nrm}{{\text{n}}}
\newcommand{\orm}{{\text{o}}}
\newcommand{\prm}{{\text{p}}}
\newcommand{\qrm}{{\text{q}}}
\newcommand{\rrm}{{\text{r}}}
\newcommand{\srm}{{\text{s}}}
\newcommand{\trm}{{\text{t}}}
\newcommand{\urm}{{\text{u}}}
\newcommand{\vrm}{{\text{v}}}
\newcommand{\wrm}{{\text{w}}}
\newcommand{\xrm}{{\text{x}}}
\newcommand{\yrm}{{\text{y}}}
\newcommand{\zrm}{{\text{z}}}
\newcommand{\alpharm}{{\upalpha}}
\newcommand{\betarm}{{\upbeta}}
\newcommand{\gammarm}{{\upgamma}}
\newcommand{\deltarm}{{\updelta}}
\newcommand{\epsilonrm}{{\upepsilon}}
\newcommand{\zetarm}{{\upzeta}}
\newcommand{\etarm}{{\upeta}}
\newcommand{\thetarm}{{\uptheta}}
\newcommand{\iotarm}{{\upiota}}
\newcommand{\kapparm}{{\upkappa}}
\newcommand{\lambdarm}{{\uplambda}}
\newcommand{\murm}{{\upmu}}
\newcommand{\nurm}{{\upnu}}
\newcommand{\xirm}{{\upxi}}
\newcommand{\pirm}{{\uppi}}
\newcommand{\rhorm}{{\uprho}}
\newcommand{\sigmarm}{{\upsigma}}
\newcommand{\taurm}{{\uptau}}
\newcommand{\upsilonrm}{{\upupsilon}}
\newcommand{\phirm}{{\upphi}}
\newcommand{\chirm}{{\upchi}}
\newcommand{\psirm}{{\uppsi}}
\newcommand{\omegarm}{{\upomega}}
\newcommand{\nablarm}{{\upnabla}}
% Bold characters (\<letter>bold)
\newcommand{\Abold}{{\boldsymbol{A}}}
\newcommand{\Bbold}{{\boldsymbol{B}}}
\newcommand{\Cbold}{{\boldsymbol{C}}}
\newcommand{\Dbold}{{\boldsymbol{D}}}
\newcommand{\Ebold}{{\boldsymbol{E}}}
\newcommand{\Fbold}{{\boldsymbol{F}}}
\newcommand{\Gbold}{{\boldsymbol{G}}}
\newcommand{\Hbold}{{\boldsymbol{H}}}
\newcommand{\Ibold}{{\boldsymbol{I}}}
\newcommand{\Jbold}{{\boldsymbol{J}}}
\newcommand{\Kbold}{{\boldsymbol{K}}}
\newcommand{\Lbold}{{\boldsymbol{L}}}
\newcommand{\Mbold}{{\boldsymbol{M}}}
\newcommand{\Nbold}{{\boldsymbol{N}}}
\newcommand{\Obold}{{\boldsymbol{O}}}
\newcommand{\Pbold}{{\boldsymbol{P}}}
\newcommand{\Qbold}{{\boldsymbol{Q}}}
\newcommand{\Rbold}{{\boldsymbol{R}}}
\newcommand{\Sbold}{{\boldsymbol{S}}}
\newcommand{\Tbold}{{\boldsymbol{T}}}
\newcommand{\Ubold}{{\boldsymbol{U}}}
\newcommand{\Vbold}{{\boldsymbol{V}}}
\newcommand{\Wbold}{{\boldsymbol{W}}}
\newcommand{\Xbold}{{\boldsymbol{X}}}
\newcommand{\Ybold}{{\boldsymbol{Y}}}
\newcommand{\Zbold}{{\boldsymbol{Z}}}
\newcommand{\Alphabold}{{\boldsymbol{\Alpha}}}
\newcommand{\Betabold}{{\boldsymbol{\Beta}}}
\newcommand{\Gammabold}{{\boldsymbol{\Gamma}}}
\newcommand{\Deltabold}{{\boldsymbol{\Delta}}}
\newcommand{\Epsilonbold}{{\boldsymbol{\Epsilon}}}
\newcommand{\Zetabold}{{\boldsymbol{\Zeta}}}
\newcommand{\Etabold}{{\boldsymbol{\Eta}}}
\newcommand{\Thetabold}{{\boldsymbol{\Theta}}}
\newcommand{\Iotabold}{{\boldsymbol{\Iota}}}
\newcommand{\Kappabold}{{\boldsymbol{\Kappa}}}
\newcommand{\Lambdabold}{{\boldsymbol{\Lambda}}}
\newcommand{\Mubold}{{\boldsymbol{\Mu}}}
\newcommand{\Nubold}{{\boldsymbol{\Nu}}}
\newcommand{\Xibold}{{\boldsymbol{\Xi}}}
\newcommand{\Pibold}{{\boldsymbol{\Pi}}}
\newcommand{\Rhobold}{{\boldsymbol{\Rho}}}
\newcommand{\Sigmabold}{{\boldsymbol{\Sigma}}}
\newcommand{\Taubold}{{\boldsymbol{\Tau}}}
\newcommand{\Upsilonbold}{{\boldsymbol{\Upsilon}}}
\newcommand{\Phibold}{{\boldsymbol{\Phi}}}
\newcommand{\Chibold}{{\boldsymbol{\Chi}}}
\newcommand{\Psibold}{{\boldsymbol{\Psi}}}
\newcommand{\Omegabold}{{\boldsymbol{\Omega}}}
\newcommand{\abold}{{\boldsymbol{a}}}
\newcommand{\bbold}{{\boldsymbol{b}}}
\newcommand{\cbold}{{\boldsymbol{c}}}
\newcommand{\dbold}{{\boldsymbol{d}}}
\newcommand{\ebold}{{\boldsymbol{e}}}
\newcommand{\fbold}{{\boldsymbol{f}}}
\newcommand{\gbold}{{\boldsymbol{g}}}
\newcommand{\hbold}{{\boldsymbol{h}}}
\newcommand{\ibold}{{\boldsymbol{i}}}
\newcommand{\jbold}{{\boldsymbol{j}}}
\newcommand{\kbold}{{\boldsymbol{k}}}
\newcommand{\lbold}{{\boldsymbol{l}}}
\newcommand{\mbold}{{\boldsymbol{m}}}
\newcommand{\nbold}{{\boldsymbol{n}}}
\newcommand{\obold}{{\boldsymbol{o}}}
\newcommand{\pbold}{{\boldsymbol{p}}}
\newcommand{\qbold}{{\boldsymbol{q}}}
\newcommand{\rbold}{{\boldsymbol{r}}}
\newcommand{\sbold}{{\boldsymbol{s}}}
\newcommand{\tbold}{{\boldsymbol{t}}}
\newcommand{\ubold}{{\boldsymbol{u}}}
\newcommand{\vbold}{{\boldsymbol{v}}}
\newcommand{\wbold}{{\boldsymbol{w}}}
\newcommand{\xbold}{{\boldsymbol{x}}}
\newcommand{\ybold}{{\boldsymbol{y}}}
\newcommand{\zbold}{{\boldsymbol{z}}}
\newcommand{\alphabold}{{\boldsymbol{\alpha}}}
\newcommand{\betabold}{{\boldsymbol{\beta}}}
\newcommand{\gammabold}{{\boldsymbol{\gamma}}}
\newcommand{\deltabold}{{\boldsymbol{\delta}}}
\newcommand{\epsilonbold}{{\boldsymbol{\epsilon}}}
\newcommand{\zetabold}{{\boldsymbol{\zeta}}}
\newcommand{\etabold}{{\boldsymbol{\eta}}}
\newcommand{\thetabold}{{\boldsymbol{\theta}}}
\newcommand{\iotabold}{{\boldsymbol{\iota}}}
\newcommand{\kappabold}{{\boldsymbol{\kappa}}}
\newcommand{\lambdabold}{{\boldsymbol{\lambda}}}
\newcommand{\mubold}{{\boldsymbol{\mu}}}
\newcommand{\nubold}{{\boldsymbol{\nu}}}
\newcommand{\xibold}{{\boldsymbol{\xi}}}
\newcommand{\pibold}{{\boldsymbol{\pi}}}
\newcommand{\rhobold}{{\boldsymbol{\rho}}}
\newcommand{\sigmabold}{{\boldsymbol{\sigma}}}
\newcommand{\taubold}{{\boldsymbol{\tau}}}
\newcommand{\upsilonbold}{{\boldsymbol{\upsilon}}}
\newcommand{\phibold}{{\boldsymbol{\phi}}}
\newcommand{\chibold}{{\boldsymbol{\chi}}}
\newcommand{\psibold}{{\boldsymbol{\psi}}}
\newcommand{\omegabold}{{\boldsymbol{\omega}}}
\newcommand{\nablabold}{{\boldsymbol{\nabla}}}
% Fancy script character (\<letter>cl)
\newcommand{\Acl}{{\mathcal{A}}}
\newcommand{\Bcl}{{\mathcal{B}}}
\newcommand{\Ccl}{{\mathcal{C}}}
\newcommand{\Dcl}{{\mathcal{D}}}
\newcommand{\Ecl}{{\mathcal{E}}}
\newcommand{\Fcl}{{\mathcal{F}}}
\newcommand{\Gcl}{{\mathcal{G}}}
\newcommand{\Hcl}{{\mathcal{H}}}
\newcommand{\Icl}{{\mathcal{I}}}
\newcommand{\Jcl}{{\mathcal{J}}}
\newcommand{\Kcl}{{\mathcal{K}}}
\newcommand{\Lcl}{{\mathcal{L}}}
\newcommand{\Mcl}{{\mathcal{M}}}
\newcommand{\Ncl}{{\mathcal{N}}}
\newcommand{\Ocl}{{\mathcal{O}}}
\newcommand{\Pcl}{{\mathcal{P}}}
\newcommand{\Qcl}{{\mathcal{Q}}}
\newcommand{\Rcl}{{\mathcal{R}}}
\newcommand{\Scl}{{\mathcal{S}}}
\newcommand{\Tcl}{{\mathcal{T}}}
\newcommand{\Ucl}{{\mathcal{U}}}
\newcommand{\Vcl}{{\mathcal{V}}}
\newcommand{\Wcl}{{\mathcal{W}}}
\newcommand{\Xcl}{{\mathcal{X}}}
\newcommand{\Ycl}{{\mathcal{Y}}}
\newcommand{\Zcl}{{\mathcal{Z}}}
% Bold series matrices (\<letter>brm)
\newcommand{\Abrm}{{\textrm{\bfseries A}}}
\newcommand{\Bbrm}{{\textrm{\bfseries B}}}
\newcommand{\Cbrm}{{\textrm{\bfseries C}}}
\newcommand{\Dbrm}{{\textrm{\bfseries D}}}
\newcommand{\Ebrm}{{\textrm{\bfseries E}}}
\newcommand{\Fbrm}{{\textrm{\bfseries F}}}
\newcommand{\Gbrm}{{\textrm{\bfseries G}}}
\newcommand{\Hbrm}{{\textrm{\bfseries H}}}
\newcommand{\Ibrm}{{\textrm{\bfseries I}}}
\newcommand{\Jbrm}{{\textrm{\bfseries J}}}
\newcommand{\Kbrm}{{\textrm{\bfseries K}}}
\newcommand{\Lbrm}{{\textrm{\bfseries L}}}
\newcommand{\Mbrm}{{\textrm{\bfseries M}}}
\newcommand{\Nbrm}{{\textrm{\bfseries N}}}
\newcommand{\Obrm}{{\textrm{\bfseries O}}}
\newcommand{\Pbrm}{{\textrm{\bfseries P}}}
\newcommand{\Qbrm}{{\textrm{\bfseries Q}}}
\newcommand{\Rbrm}{{\textrm{\bfseries R}}}
\newcommand{\Sbrm}{{\textrm{\bfseries S}}}
\newcommand{\Tbrm}{{\textrm{\bfseries T}}}
\newcommand{\Ubrm}{{\textrm{\bfseries U}}}
\newcommand{\Vbrm}{{\textrm{\bfseries V}}}
\newcommand{\Wbrm}{{\textrm{\bfseries W}}}
\newcommand{\Xbrm}{{\textrm{\bfseries X}}}
\newcommand{\Ybrm}{{\textrm{\bfseries Y}}}
\newcommand{\Zbrm}{{\textrm{\bfseries Z}}}
\newcommand{\Alphabrm}{{\boldsymbol{\upAlpha}}}
\newcommand{\Betabrm}{{\boldsymbol{\upBeta}}}
\newcommand{\Gammabrm}{{\boldsymbol{\upGamma}}}
\newcommand{\Deltabrm}{{\boldsymbol{\upDelta}}}
\newcommand{\Epsilonbrm}{{\boldsymbol{\upEpsilon}}}
\newcommand{\Zetabrm}{{\boldsymbol{\upZeta}}}
\newcommand{\Etabrm}{{\boldsymbol{\upEta}}}
\newcommand{\Thetabrm}{{\boldsymbol{\upTheta}}}
\newcommand{\Iotabrm}{{\boldsymbol{\upIota}}}
\newcommand{\Kappabrm}{{\boldsymbol{\upKappa}}}
\newcommand{\Lambdabrm}{{\boldsymbol{\upLambda}}}
\newcommand{\Mubrm}{{\boldsymbol{\upMu}}}
\newcommand{\Nubrm}{{\boldsymbol{\upNu}}}
\newcommand{\Xibrm}{{\boldsymbol{\upXi}}}
\newcommand{\Pibrm}{{\boldsymbol{\upPi}}}
\newcommand{\Rhobrm}{{\boldsymbol{\upRho}}}
\newcommand{\Sigmabrm}{{\boldsymbol{\upSigma}}}
\newcommand{\Taubrm}{{\boldsymbol{\upTau}}}
\newcommand{\Upsilonbrm}{{\boldsymbol{\upUpsilon}}}
\newcommand{\Phibrm}{{\boldsymbol{\upPhi}}}
\newcommand{\Chibrm}{{\boldsymbol{\upChi}}}
\newcommand{\Psibrm}{{\boldsymbol{\upPsi}}}
\newcommand{\Omegabrm}{{\boldsymbol{\upOmega}}}
% Bold series vectors (\<letter>brm)
\newcommand{\abrm}{{\textrm{\bfseries a}}}
\newcommand{\bbrm}{{\textrm{\bfseries b}}}
\newcommand{\cbrm}{{\textrm{\bfseries c}}}
\newcommand{\dbrm}{{\textrm{\bfseries d}}}
\newcommand{\ebrm}{{\textrm{\bfseries e}}}
\newcommand{\fbrm}{{\textrm{\bfseries f}}}
\newcommand{\gbrm}{{\textrm{\bfseries g}}}
\newcommand{\hbrm}{{\textrm{\bfseries h}}}
\newcommand{\ibrm}{{\textrm{\bfseries i}}}
\newcommand{\jbrm}{{\textrm{\bfseries j}}}
\newcommand{\kbrm}{{\textrm{\bfseries k}}}
\newcommand{\lbrm}{{\textrm{\bfseries l}}}
\newcommand{\mbrm}{{\textrm{\bfseries m}}}
\newcommand{\nbrm}{{\textrm{\bfseries n}}}
\newcommand{\obrm}{{\textrm{\bfseries o}}}
\newcommand{\pbrm}{{\textrm{\bfseries p}}}
\newcommand{\qbrm}{{\textrm{\bfseries q}}}
\newcommand{\rbrm}{{\textrm{\bfseries r}}}
\newcommand{\sbrm}{{\textrm{\bfseries s}}}
\newcommand{\tbrm}{{\textrm{\bfseries t}}}
\newcommand{\ubrm}{{\textrm{\bfseries u}}}
\newcommand{\vbrm}{{\textrm{\bfseries v}}}
\newcommand{\wbrm}{{\textrm{\bfseries w}}}
\newcommand{\xbrm}{{\textrm{\bfseries x}}}
\newcommand{\ybrm}{{\textrm{\bfseries y}}}
\newcommand{\zbrm}{{\textrm{\bfseries z}}}
\newcommand{\alphabrm}{{\boldsymbol{\upalpha}}}
\newcommand{\betabrm}{{\boldsymbol{\upbeta}}}
\newcommand{\gammabrm}{{\boldsymbol{\upgamma}}}
\newcommand{\deltabrm}{{\boldsymbol{\updelta}}}
\newcommand{\epsilonbrm}{{\boldsymbol{\upepsilon}}}
\newcommand{\zetabrm}{{\boldsymbol{\upzeta}}}
\newcommand{\etabrm}{{\boldsymbol{\upeta}}}
\newcommand{\thetabrm}{{\boldsymbol{\uptheta}}}
\newcommand{\iotabrm}{{\boldsymbol{\upiota}}}
\newcommand{\kappabrm}{{\boldsymbol{\upkappa}}}
\newcommand{\lambdabrm}{{\boldsymbol{\uplambda}}}
\newcommand{\mubrm}{{\boldsymbol{\upmu}}}
\newcommand{\nubrm}{{\boldsymbol{\upnu}}}
\newcommand{\xibrm}{{\boldsymbol{\upxi}}}
\newcommand{\pibrm}{{\boldsymbol{\uppi}}}
\newcommand{\rhobrm}{{\boldsymbol{\uprho}}}
\newcommand{\sigmabrm}{{\boldsymbol{\upsigma}}}
\newcommand{\taubrm}{{\boldsymbol{\uptau}}}
\newcommand{\upsilonbrm}{{\boldsymbol{\upupsilon}}}
\newcommand{\phibrm}{{\boldsymbol{\upphi}}}
\newcommand{\chibrm}{{\boldsymbol{\upchi}}}
\newcommand{\psibrm}{{\boldsymbol{\uppsi}}}
\newcommand{\omegabrm}{{\boldsymbol{\upomega}}}
% vectors with line over (\<letter>v)
\newcommand{\Av}{{\vec{A}}}
\newcommand{\Bv}{{\vec{B}}}
\newcommand{\Cv}{{\vec{C}}}
\newcommand{\Dv}{{\vec{D}}}
\newcommand{\Ev}{{\vec{E}}}
\newcommand{\Fv}{{\vec{F}}}
\newcommand{\Gv}{{\vec{G}}}
\newcommand{\Hv}{{\vec{H}}}
\newcommand{\Iv}{{\vec{I}}}
\newcommand{\Jv}{{\vec{J}}}
\newcommand{\Kv}{{\vec{K}}}
\newcommand{\Lv}{{\vec{L}}}
\newcommand{\Mv}{{\vec{M}}}
\newcommand{\Nv}{{\vec{N}}}
\newcommand{\Ov}{{\vec{O}}}
\newcommand{\Pv}{{\vec{P}}}
\newcommand{\Qv}{{\vec{Q}}}
\newcommand{\Rv}{{\vec{R}}}
\newcommand{\Sv}{{\vec{S}}}
\newcommand{\Tv}{{\vec{T}}}
\newcommand{\Uv}{{\vec{U}}}
\newcommand{\Vv}{{\vec{V}}}
\newcommand{\Wv}{{\vec{W}}}
\newcommand{\Xv}{{\vec{X}}}
\newcommand{\Yv}{{\vec{Y}}}
\newcommand{\Zv}{{\vec{Z}}}
\newcommand{\Alphav}{{\vec{\Alpha}}}
\newcommand{\Betav}{{\vec{\Beta}}}
\newcommand{\Gammav}{{\vec{\Gamma}}}
\newcommand{\Deltav}{{\vec{\Delta}}}
\newcommand{\Epsilonv}{{\vec{\Epsilon}}}
\newcommand{\Zetav}{{\vec{\Zeta}}}
\newcommand{\Etav}{{\vec{\Eta}}}
\newcommand{\Thetav}{{\vec{\Theta}}}
\newcommand{\Iotav}{{\vec{\Iota}}}
\newcommand{\Kappav}{{\vec{\Kappa}}}
\newcommand{\Lambdav}{{\vec{\Lambda}}}
\newcommand{\Muv}{{\vec{\Mu}}}
\newcommand{\Nuv}{{\vec{\Nu}}}
\newcommand{\Xiv}{{\vec{\Xi}}}
\newcommand{\Piv}{{\vec{\Pi}}}
\newcommand{\Rhov}{{\vec{\Rho}}}
\newcommand{\Sigmav}{{\vec{\Sigma}}}
\newcommand{\Tauv}{{\vec{\Tau}}}
\newcommand{\Upsilonv}{{\vec{\Upsilon}}}
\newcommand{\Phiv}{{\vec{\Phi}}}
\newcommand{\Chiv}{{\vec{\Chi}}}
\newcommand{\Psiv}{{\vec{\Psi}}}
\newcommand{\Omegav}{{\vec{\Omega}}}
\newcommand{\av}{{\vec{a}}}
\newcommand{\bv}{{\vec{b}}}
\newcommand{\cv}{{\vec{c}}}
\newcommand{\dv}{{\vec{d}}}
\newcommand{\ev}{{\vec{e}}}
\newcommand{\fv}{{\vec{f}}}
\newcommand{\gv}{{\vec{g}}}
\newcommand{\hv}{{\vec{h}}}
\newcommand{\iv}{{\vec{i}}}
\newcommand{\jv}{{\vec{j}}}
\newcommand{\kv}{{\vec{k}}}
\newcommand{\lv}{{\vec{l}}}
\newcommand{\mv}{{\vec{m}}}
\newcommand{\nv}{{\vec{n}}}
\newcommand{\ov}{{\vec{o}}}
\newcommand{\pv}{{\vec{p}}}
\newcommand{\qv}{{\vec{q}}}
\newcommand{\rv}{{\vec{r}}}
\newcommand{\sv}{{\vec{s}}}
\newcommand{\tv}{{\vec{t}}}
\newcommand{\uv}{{\vec{u}}}
\newcommand{\vv}{{\vec{v}}}
\newcommand{\wv}{{\vec{w}}}
\newcommand{\xv}{{\vec{x}}}
\newcommand{\yv}{{\vec{y}}}
\newcommand{\zv}{{\vec{z}}}
\newcommand{\alphav}{{\vec{\alpha}}}
\newcommand{\betav}{{\vec{\beta}}}
\newcommand{\gammav}{{\vec{\gamma}}}
\newcommand{\deltav}{{\vec{\delta}}}
\newcommand{\epsilonv}{{\vec{\epsilon}}}
\newcommand{\zetav}{{\vec{\zeta}}}
\newcommand{\etav}{{\vec{\eta}}}
\newcommand{\thetav}{{\vec{\theta}}}
\newcommand{\iotav}{{\vec{\iota}}}
\newcommand{\kappav}{{\vec{\kappa}}}
\newcommand{\lambdav}{{\vec{\lambda}}}
\newcommand{\muv}{{\vec{\mu}}}
\newcommand{\nuv}{{\vec{\nu}}}
\newcommand{\xiv}{{\vec{\xi}}}
\newcommand{\piv}{{\vec{\pi}}}
\newcommand{\rhov}{{\vec{\rho}}}
\newcommand{\sigmav}{{\vec{\sigma}}}
\newcommand{\tauv}{{\vec{\tau}}}
\newcommand{\upsilonv}{{\vec{\upsilon}}}
\newcommand{\phiv}{{\vec{\phi}}}
\newcommand{\chiv}{{\vec{\chi}}}
\newcommand{\psiv}{{\vec{\psi}}}
\newcommand{\omegav}{{\vec{\omega}}}
\newcommand{\nablav}{{\vec{\nabla}}}
% special vectors
\newcommand{\normvec}{\hat{\nbrm}}
\newcommand{\tangvec}{\hat{\tbrm}}
\newcommand{\eboldd}{{\ebold^d}}
\newcommand{\zerobold}{{\boldsymbol{0}}}

\newcommand{\Dim}{D}

%%% General math
\newcommand{\IntSpace}{\mathbb{Z}}
\newcommand{\RealSpace}{\mathbb{R}}
\newcommand{\ComplexSpace}{\mathbb{C}}

%%% Linear algebra
\newcommand{\tp}[1]{{#1}^{\intercal}} %matrix transpose
\newcommand{\inv}[1]{{#1}^{-1}} %matrix inverse
\newcommand{\pinv}[1]{{#1}^{\dagger}} %matrix pseudo inverse
\newcommand{\Iden}{\mathbb{I}} % identity operator
\newcommand{\tensor}[1]{\vec{\vec{#1}}}

% Full differential {numerator}{denominator}{degree}
\newcommand{\fdiff}[3]{\ifnum#3=1\ensuremath{\frac{\mathrm{d} #1}{\mathrm{d} #2}}\else\ensuremath{\frac{\mathrm{d}^{#3} #1}{\mathrm{d} #2^{#3}}}\fi}
% Full partial {numerator}{denominator}{degree}
\newcommand*{\fpartial}[3]{\ifnum#3=1\ensuremath{\frac{\partial #1}{\partial #2}}\else\ensuremath{\frac{\partial^{#3} #1}{\partial #2^{#3}}}\fi}
% Full partial {numerator}{denominator}{degree} with extra spacing for denom
\newcommand*{\fpartialspc}[3]{\ifnum#3=1\ensuremath{\frac{\partial #1}{\partial #2}}\else\ensuremath{\frac{\partial^{#3} #1}{\partial #2^{\,#3}}}\fi}
% Full mixed partial {numerator}{denom1}{degree1}{denom2}{degree2}
\newcounter{npdeg}
\newcommand*{\fmpartial}[5]{\setcounter{npdeg}{#3}\addtocounter{npdeg}{#5}\ensuremath{\frac{\partial^{\thenpdeg} #1}{\partial{#2}\ifnum#3>1^{#3}\fi\partial{#4}\ifnum#5>1^{#5}\fi}}}

% Cell step subscript (argument is '+n'+ or '-n')
\newcommand*{\hstep}[1]{\ensuremath{{\ibold#1\eboldd}}}
% Cell half step subscript (argument is '+' or '-')
\newcommand*{\hhstep}[1]{\ensuremath{{\ibold#1\frac{1}{2}\eboldd}}}
% Cell half step subscript (argument is '+' or '-'), second is number of halves
\newcommand*{\hhstepn}[2]{\ensuremath{{\ibold#1\frac{#2}{2}\eboldd}}}
% Average angle brackets
\newcommand*{\avg}[1]{\ensuremath{\langle #1\rangle}}

%--Spacing
\newcommand{\zapspace}{\topsep=1pt\partopsep=1pt\itemsep=1pt\parskip=2pt}

%--Rules

 % Defines the horizontal lines in the front page, change thickness here
\newcommand{\hruletitle}{\rule{\linewidth}{.6mm}}

\makeatother

\endinput
