\chapter{High-Order Mappings from Arbitrary Discrete Sources}
The focus of this chapter is to develop a method which enables any Chombo-based application to work with any arbitrary geometry block specified as a discrete structured grid, while maintaining fourth-order accuracy and AMR.

Currently, the constraint to analytic geometry in Chombo exists for two reasons.
First, Chombo incorporates many stencil based technologies (including domain specific languages\cite{Guzik2016}) that are necessarily applied to Cartesian grids.
Use of Cartesian grids requires a mapping to be created which transforms between a simple Cartesian block and some interesting geometry, as illustrated by \figref{fig:mapping}.
This transformation can be defined using an analytic function, and any solution accuracy is maintained.
However, grids are frequently created using non-analytic methods, thus allowing significantly more complex mappings to occur.
The drawback of non-analytic functions is they are only defined at a discrete number of points, and require more care in order to maintain solution accuracy.
For this reason Chombo uses analytic function mappings that are defined everywhere within the domain.

For Chombo to handle an arbitrary geometry, a method must be developed to use either analytic mapping or any discretely defined mapping~\cite{Henshaw1996b}.
What may seem an obvious solution at first is to simply generate a grid mapping at the finest level of refinement everywhere, and then only use the points that are needed.
This solution, although simple, defeats the purpose of AMR since mappings for a large number of unnecessary nodes would be stored.
An alternative method for AMR with discrete mappings is to interpolate the mapping function at points needed.
Since Chord is a fourth-order code, this interpolation requires some care in order to maintain the solution accuracy.
\figref{fig:HOmappedAMR} illustrates this concept where the black grid shows a true mapping and the gray is a linear approximation.
It is evident that significant error can be present in refined nodes, and for high accuracy a better approximation must be made.
Use of an interpolating b-spline, or basis spline, is proposed, as it is efficient and can be created at any chosen order of accuracy.

Prior work on block based AMR has been done by Steinthorsson et.
at.~\cite{Steinthorsson1994} at second order accuracy.
The solution developed uses a hybrid of splines for interpolation along coarse grid lines, and Hermite interpolation for refinement off the coarse grid lines.
Although effective, this method does not easily transfer to high-order and has continuity challenges between coarse and fine levels.
The concept of using b-splines for interpolation of grid based data is by no means a new idea, and has been studied by many, such as de Boor~\cite{DeBoor} and Piegl~\cite{Piegl1997}.
Further study of b-splines for a number of applications to CFD simulations, such as zonal grids and Galerkin methods, has been done by Karvchenko et at.~\cite{Kravchenko1999} where b-splines are concluded to be highly effective for various forms of interpolation.

\begin{figure}[!h]
  \centering
  \includegraphics[width=3.75in]{figures/AMR}
  \caption{Visualization of why a high-order fitting is need for AMR.
It is clear the linear approximation has significant error from the true refinement locations, motivating need for higher order interpolation.} % The squares are points of the coarse mesh, gray circles showing linear approximation to refined nodes, and the white the true position which should be closely approximated.
  \label{fig:HOmappedAMR}
\end{figure}

This paper begins by providing background on the scheme used by Chord for working with mapped grids.
The first part of this section explains how grid metric terms fit into a fourth-order finite-volume scheme, while the second describes the grid metrics.
Following the background portion is exploration of interpolation methods in section \ref{spline:sec:interp}.
In this section the requirements for an interpolation scheme are first developed.
Following is the simple case using piecewise Hermite interpolation, which although a good starting point is ultimately unfit for our needs.
A more advanced method, spline interpolation, is then explored.
Section \ref{spline:sec:results} verifies the use of b-splines for interpolation, and presents results of a case demonstrating the use of the spline interpolation method for discrete grid mappings.
Solving the Euler equations to advect a Gaussian ``bump'' in density is used for comparing results between a well known analytic mapping, and its discretized form utilizing spline interpolation.
Convergence and conservation test are performed.
The first of these is an advection case used for comparing results between a well known analytic mapping and its discretized form utilizing spline interpolation.
The second case is that of a backward facing step, demonstrating some of the possibilities interpolated grid metrics will allow for.
Finally, some remarks and areas for future work are given.


\section{Results and Discussion} \label{spline:sec:results}

\subsection{Test of B-Spline Order of Accuracy}
One of the main challenges for the development of a fourth-order AMR algorithm on arbitrary mapped grids is maintaining the order of accuracy.
In the development of spline theory, splines are claimed to have order of accuracy $p$, and validation of this takes place.

    %     Since a b-spline is construed by a set of polynomials, it is expected to reconstruct a polynomial of that order exactly.
Running tests this is observed, and the results are uninteresting so excluded here.

Implementation of 1D cubic and quintic splines can be validated against any smooth function, ideally of reasonable complexity.
Convergence tests are performed using a sample function, which happens to be a viscosity-temperature relation, with uniform interpolation points and results shown in \figref{fig:splineConverg}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.02in]{figures/cubic}
    \caption{Cubic spline.}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.02in]{figures/quintic}
    \caption{Quintic spline.}
  \end{subfigure}
  \caption{Convergence rate of interpolation b-splines for the function $f(x) = \exp(0.746 \log{x} + \frac{43.555}{x} - \frac{3259.934}{x^2} + 0.13556)$ with the number of interpolation points uniformly spaced over $x\in [300, 700]$}
  \label{fig:splineConverg}
\end{figure}

Results of this convergence test show the expected order of accuracy for both the cubic and quintic splines are observed.
The cubic spline maintains a convergence rate of 4.0.
The quintic spline exhibits a convergence rate between the 6.0 and 7.0.
This verifies 1D b-splines, and likewise multi-dimensional b-splines since they simply apply recursion to one-dimensional problems, and thus are suitable for high order grid interpolation.

    %     \subsection{Verification of One-Sided metrics in Chombo}
    %     Chombo, in its given state, is only equipped to handle analytic geometries.
In order to allow for the use of arbitrary geometries splines will take the place of the analytic mapping, and thus the majority of the functionality can remain largely the same.
One issue however is that for an analytic geometry metrics are defined everywhere, while for a spline they should be defined strictly within the domain.
This becomes an issue in Chombo, as grid metric terms are freely used within ghost cells where they can not be defined.
To correct this, Chombo has been adjusted (but apparently this is incomplete) to utilize one sided calculations for all metric related calculation along domain bounds.
This correction applies to all boundary conditions, with the exception of periodic boundaries where metrics are now exchanged since ghost cell fall within the domain.
Some verification of this adjustment is done to ensure that Chombo retains its fourth-order accuracy.


    %     For this test case, the couette flow case is used since it has a known exact solution and contains both a wall and periodic boundary.
The exact solution of reference is at $t=32$, and coarse $\Delta t = 1*10^{-3}$ is chosen.
The convergence rates are then compared, although since they are within $1\%$ difference a plot of this is meaningless.

    %     \begin{figure}
    %     \centering
    %     \begin{tabular}{c|ccc}
    %           & Ghost metrics & One sided & difference \\ \hline
    %     $n=32$ & 3.34308e-08 & 3.34308e-08 & 0.0 \\
    %     $n=64$ & 2.07187e-09 & 2.07185e-09 & 1.71865e-14 \\
    %     $n=128$ &1.32335e-10 & 1.32335e-10 & 0.0
                                               %   \end{tabular}
                                               %                                                \caption{$L_2$ norms without AMR, identical convergence rates near 4.0 are observed.}
                                               %                                                \end{figure}
                                               %                                                First testing without AMR it is observed that the errors, and thus convergence rates, before and after changes are identical.
This is ideal, since the use of one-sided calculations should not cause any loss of accuracy.
For this case it is noted that the order of accuracy is measured as close to 4.0 for each of the $L_1$, $L_2$, and $L_{inf}$ norms.

                                               %                                                \begin{figure}
                                               %                                                \centering
                                               %                                                \begin{tabular}{c|ccc}
                                               %                 & Ghost metrics & One sided & difference \\ \hline
    %     $n=32$ &  5.97220e-08 & 5.91862e-08 & -5.35706e-10 \\
    %     $n=64$ &  7.79734e-09 & 7.82435e-09 &  2.70218e-11 \\
    %     $n=128$ & 1.32525e-09 & 1.33455e-09 &  9.29478e-12
                                                %   \end{tabular}
                                                %                                                 \caption{$L_2$ norms with pseudo-AMR, observe convergence rate of 2.9372 (using ghost metrics) vs 2.9192 (using one sided).}
                                                %                                                 \end{figure}

                                                %                                                 For the case with AMR, as small region of AMR is fixed and set to alternate expanding or reduce by 4 coarse cells every $10^{-2}$ seconds.
This number of cells is scaled for increasing refinement to be the same physical area.
Since this AMR region is placed arbitrarily and across gradients, it is expected that the solution lose an order of accuracy.
This is seen the the results, and the error caused by one-sided methods for grid metrics is seen to be within a range expected.
This shows (although not conclusively since other errors have since arisen) that the order of accuracy of Chombo is unaffected by the adjustment made to use one-sided derivatives.

                                                %                                                 \begin{figure}
                                                %                                                 \centering
                                                %                                                 \includegraphics[width=3in]{figures/sampleGrid}
                                                %                                                 \caption{A sample grid used for testing the convergence rate.}
                                                %                                                 \end{figure}

\subsection{Gaussian Advection Cube}
The Gaussian advection case is chosen as a test for validation of geometry implementation, as seen by Guzik et al.~\cite{Guzik2015}.
Using the Euler equations on a mapped grid with periodic boundary conditions, a Gaussian density bump is initialized with a specified flow velocity.
The density profile is initialized as
%%
\begin{equation}
  \rho = \rho_0 + s(r)\, \Delta\rho\, \mathrm{e}^{-(100 r^2)} ,
\end{equation}
%%
where
\begin{equation}
  s(r) =
  \begin{cases}
    \hfill 0  &:\quad |2r^2| >= 1\\
    \cos^6(\pi r^2) &:\quad |2r^2| < 1
  \end{cases}\,,
\end{equation}
%%
$\rho_0 = 1.4$, and $\Delta\rho = 0.14$.
The value $r$ specifies the distance from the center of the periodic domain, $[0, 1]^D$.
Pressure is initialized as a constant of 1 and the velocity is set to $(1.0, 0.5)$.
With this choice of velocity the Gaussian profile will return to the initial location after 2 time periods, and solution error is calculated using the exact solution.
The mapping is defined by
%%
\begin{equation}
  x_d = \xi_d + c_d\prod_{p=1}^D\sin(2\pi\xi_p)\,\qquad d=1,2\,.
  \label{eq:warpedmapping}
\end{equation}
%%

Initialization of this case is shown in \figref{fig:advect} in both computational and physical space.
Boxes, a collection of cells, are shown to describe the mesh with two levels of AMR applied.
The refinement is set analytically such that the first level is an approximate circle of radius $0.35$ from the center of the Gaussian bump, and the second level is of radius $0.225$ from the center.
This is chosen such that, when performing a convergence test, the refinement regions cover nearly the same area regardless of base grid.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.0in]{figures/WarpedMappedAdvectComp}
    \caption{Computational space.}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.0in]{figures/WarpedMappedAdvectPhy}
    \caption{Physical space.}
  \end{subfigure}
  \caption{Advection case initial Gaussian density profile.
    Boxes show the grid with gray the first level of refinement and black the second.}
  \label{fig:advect}
\end{figure}

                      %                       For future comparison this case would be run using several levels of AMR with the flow properties prescribed above.
Using the defined analytic mapping above, a discrete grid would also be created with the same base cell size in order to compare with the result of the b-spline interpolation method.
Application of the spline interpolation method will be shown similar to \figref{fig:advect}, allowing for the use of AMR on this grid.
A direct comparison of error will be made with the known exact solution at every 2 time periods.
This is to be done to ensure the spline interpolation method does not have significantly more error than the analytic mapping.
A study of the solution error with progressive refinement will also be done, allowing for proof the spline method also remains fourth-order accurate.
This comparison should allow for conclusive results that the spline interpolation method proposed is sufficient, and if not show potential areas of issue.
These aspects will be included in the final manuscript.

A convergence study is performed to analyze the effectiveness of using splines to recover geometry representation.
Using the mapping described, two sets of convergence test are run, with the first using the analytically defined geometry, and the second using a discretized form.
Quintic spline interpolation is applied to the discretized geometry and used to provide grid information as needed.
Using the analytic and interpolated geometry, a convergence test is run both with and without AMR applied.
The cases with AMR applied have two layers of refinement by 2, where, when compared, the most refined grid matches the base grid of the single level cases.
Ie., a $64 \times 64$ grid with two levels of AMR has a refined region matching the single level $256 \times 256$ grid.
All single level convergence rates are measured at 4.0 as expected.
The addition of AMR does not significantly reduce convergence rates, as they are measured between 3.78 and 4.0 independent of mapping.
It is expected that the discontinuous grid created by AMR can induce a reduction in accuracy by up to one order.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.02in]{figures/advectConverg}
    \caption{Analytic mapping.}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=3.02in]{figures/advectConvergSpline}
    \caption{Interpolated mapping.}
  \end{subfigure}
  \caption{Convergence rates of the advection case.
    The Single-Level $L_2$ and $L_{\inf}$ plots are overlapped by the corresponding 3-Level-AMR plots, and thus not visible.}
  \label{fig:advectConverg}
\end{figure}

Additionally, the Gaussian advection case with periodic boundaries allows for testing conservation.
A finite volume scheme with no source or sink terms does not change mass, which is tested by integration of the conservative variables in computational space $J\Ubrm$.
This is computed for initialized and completed solutions of the $512 \times 512$ AMR case, using both the analytic and interpolated mappings.
These results and the difference are tabulated in \tblref{tbl:advectConsEx} and \tblref{tbl:advectConsSp}, and it is seen that conservation is preserved to machine precision.


\begin{table}[!ht]
  \centering
  \caption{Initial and final conservative values for the advection case, using the analytic mapping}
  \begin{tabular}{c|ccc}
    $J\Ubrm$ & Initial & Final & Difference \\ \hline
    $J\rho$   & 2.299286119588755e+04 & 2.299286119588766e+04 & 0.000000000000011e+04 \\
    $J\rho u$ & 2.299286119588755e+04 & 2.299286119588766e+04 & 0.000000000000011e+04 \\
    $J\rho v$ & 1.149643059794377e+04 & 1.149643059794385e+04 & 0.000000000000008e+04 \\
    $J\rho E$ & 5.533053824742974e+04 & 5.533053824742998e+04 & 0.000000000000024e+04 \\
  \end{tabular}
  \label{tbl:advectConsEx}
\end{table}

\begin{table}[!ht]
  \centering
  \caption{Initial and final conservative values for the advection case, using the quintic spline interpolated mapping}
  \begin{tabular}{c|ccc}
    $J\Ubrm$ & Initial & Final & Difference \\ \hline
    $J\rho$   & 2.299286119578092e+04 & 2.299286119578102e+04 & 0.000000000000010e+04 \\
    $J\rho u$ & 2.299286119578092e+04 & 2.299286119578102e+04 & 0.000000000000010e+04 \\
    $J\rho v$ & 1.149643059789046e+04 & 1.149643059789051e+04 & 0.000000000000005e+04 \\
    $J\rho E$ & 5.533053824736307e+04 & 5.533053824736325e+04 & 0.000000000000018e+04 \\
  \end{tabular}
  \label{tbl:advectConsSp}
\end{table}
