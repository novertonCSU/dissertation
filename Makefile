SRC=Dissertation-Overton
SUBDIR=figures

# all: figs tex bib
all: figs tex

fast:
	pdflatex "\input{$(SRC)}"

tex:
	pdflatex "\input{$(SRC)}"
	bibtex $(SRC)
	pdflatex "\input{$(SRC)}"
	pdflatex "\input{$(SRC)}"

chapters:
	for i in [a-z]*.tex; do j=${i%.tex}; pdflatex -jobname=job_$j "\includeonly{$j}\input{$SRC}"; done

figs:
	cd figures; make

view:
	okular ${SRC}.pdf

clean:
	rm -rf *.dvi *.idx *.log *.spl *.toc *.bbl *.aux *.blg *.out *~   

allclean:
	make clean
	@for X in $(SUBDIR); do \
		$(MAKE) -C $$X clean; done
