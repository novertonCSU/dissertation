\chapter{Conclusions and Future Work}
\label{chap:conclusion}

\section{Conclusions}
This research has developed three methods to enable complex geometries for high-order FVMs on structured grids with AMR.
These methods include the HO-ACR method, the MMB method for arbitrary discrete grids with AMR, and the high-order EB method.
The HO-ACR and MMB methods have been implemented in Chord, a fourth-order finite-volume CFD software, and enabled modeling complex fluid dynamics involving turbulence and combustion in the presence of complex geometries.
%
AMR is a foundational feature for capturing multi-scale flows with computational efficiency in Chord.
Combusting flows are highly sensitive to changes in refinement, and interpolation overshoots near discontinuities can destabilize the overall simulation.
The HO-ACR method has been show as essential for stable chemistry in the presence of AMR by preventing these overshoots.
The HO-ACR method has been verified to ensure high-order accuracy for smooth flow problems, preserve bounds for non-smooth ones, and maintain conservation on mapped grids.
%
The MMB method for arbitrary discrete geometries has allowed Chord to model geometries such as the bluff-body combustor.
The method is capable of using discrete grids created from traditional meshing tools while still allowing for high-order AMR by constructing mapping functions using B-splines.
This differs from some high-order applications which utilize coupled analytic functions with grids.
Further, the conforming MMB method is used to provide gridding flexibility.
% The conforming multi-block has been demonstrated to operate for arbitrary geometries with potential sharp corners.
A number of strategies are proposed, implemented, and tested to allow the
high-order conforming MMB to handle arbitrary discrete geometries such as sharp corners, although with some limitations.
%
Finally, as an alternative approach to the MMB method, the high-order EB method has been implemented as a standalone code using the Chombo framework.
The high-order EB method is verified and validated, and demonstrated to solve Stokes flows for highly complex geometries.
Additionally, the method is stable in the presence of small cells, without any form of cell merging or redistribution.

%\subsection{HO-ACR Method for Adaptive Mesh Refinement}
% In the present study, the HO-ACR algorithm has been implemented, verified, and validated in a fourth-order finite-volume CFD software solving complex fluid dynamics problems with the presence of chemical reactions, shock waves, and realistic geometries.
% However, the interpolation operation required by the AMR process often induces unwanted overshoots near discontinuities which destabilizes the overall simulation, particularly for flows with combustion.
% This is one of the main motivations for the development of the HO-ACR algorithm.
% The HO-ACR method has been verified to ensure high-order accuracy for smooth flow problems, preserve bounds for non-smooth ones, and maintain conservation on mapped grids.
% On challenging problems such as the shock-induced \ce{H2}-\ce{O2} combustion, the HO-ACR method ensures better bounds-preservation in solutions even if a fine level is arbitrarily imposed across or near discontinuities.
% Furthermore, the application of the method to a \ce{C3H8}-air premixed flame in the bluff-body combustor has demonstrated that the engagement of HO-ACR is imperative to ensure the solution stability.
% Note that the chemical kinetics used in this combustion problem, involving 24-species and 66-reactions, is very stiff and the bluff-body geometry increases the stiffness through the nonlinear interactions between the fluid and flame dynamics.

% We have developed a mapped multi-block scheme that operates on discretely defined arbitrary geometries for high-order finite-volume methods with AMR.
% Mapping functions are constructed from discretely-defined grids by using B-splines which maintain high-order and allow for AMR.
% This differs from some high-order applications which utilize coupled analytic functions with grids.
% Further, the conforming mapped multi-block method is used to provide gridding flexibility.
% A number of strategies are proposed, implemented, and tested to allow the high-order multi-block method to handle arbitrary discrete geometries.
% There are some limitations to this approach concerning ghost cells, which need further investigation.
% Nevertheless, the algorithm has been verified and validated.
% Results for an application of engineering interest have been demonstrated and compare well to experimental data.


\section{Original Contributions}
This research has made novel contributions, enabling the high-order FVM algorithm to solve flow on complex geometries using structured AMR grids.
Original contributions include:
\begin{itemize}
  \item Development of the HO-ACR method for conservative bounded interpolation of averaged quantities.
        This method is required for stability of solutions with AMR and stiff physics, such as turbulent chemistry.
        This method makes approaches for embedded-LES or embedded-DNS possible for high-order finite-volume schemes, and allows greater flexibility in when and where AMR may be applied.
  \item Demonstration of high-order AMR on discretely defined structured grids by use of a B-spline mapping function.
        This allows for Chord to utilize grids from industry standard mesh generation tools with no loss of accuracy.
  \item Development of the high-order conforming multi-block method for arbitrary geometries.
        This allows complex non-analytic geometries on structured grids, while maintaining permitting AMR and maintaining solution conservation.
        This work has enabled modeling complex cases such as the bluff-body combustor in Chord.
  \item Creation of a time dependent fourth-order embedded-boundary method for the Stokes equations.
        Prior work with embedded-boundary methods has been limited to second-order in time, and fourth-order methods have only previously been shown for the Poisson equation.
\end{itemize}

\section{Future Work}
There are a number of directions for future work, such as improvements to existing algorithms and new areas of exploration.
These include:
\begin{itemize}
  \item Extending the HO-ACR method to the computational geometries represented by general mapped multi-block domains.
        This would require having some form of bound-preserving property in the interpolation operation across blocks, where the grid metrics may also be discontinuous.
        Interpolation between blocks requires a somewhat different approach for bounds preservation since no reasonable conservation constraint can be maintained.
  \item The mapped multi-block algorithm as implemented in Chord is restricted by the number of ghost cells used.
        Many ghost cells make designing grids difficult, and the data exchanges between blocks are costly.
        To alleviate the ghost cell restriction, the number of ghost cells could be reduced at the expense of more frequent data exchanges.
        This requires moderate modification to the current algorithm of Chord, but no fundamental changes to the MMB approach.
  \item Ghost cell restrictions of the MMB method may be removed altogether by instead building a reconstruction function at block interfaces.
        The reconstruction could then be incorporated into stencils near the block boundaries, and eliminate ghost cells between blocks entirely.
        This would remove the challenges and complexity of filling ghost cells, but require rethinking the MMB approach and significantly change stencils near boundaries.
  \item The high-order EB methodology has shown great promise, but has yet to be shown for more complex physics.
        An immediate goal is to expand the method developed in this work to the full Navier-Stokes equations and utilize AMR.
        To do so, the most immediate challenge is modeling the non-linear advection term.
  \item The combination of EB with MMB technologies could ideally provide the advantages of both.
        This would allow for meshes which are mapped to control grid quality for coarse features, while capable of representing small scale sharp features that only unstructured or EB meshes can capture.
        This requires additional development of the EB methodology to consider mapped coordinates.
  \item For both the MMB and EB methods, the choice of stencil greatly impacts the resulting interpolation stencils.
        Choosing large symmetric stencils as done in this dissertation is effective, but can be expensive to evaluate.
        Ideally, these stencils could be reduced automatically in a way that has no adverse effects on the stability and greatly improves computation time.
        With this goal in mind, stencil selection may be viewed from the angle of a basis pursuit problem.
        For MMB applications, this approach may be able to detect when grids conform and correctly reduce the stencil interpolation.
        The EB method could take advantage of this to prune stencils in ways appropriate for the particular operator being evaluated.
\end{itemize}
